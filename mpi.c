#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <mpi.h>
#include <time.h>

/** Funcao que ira exibir o tabuleiro com valores 1 e 0, 
 *  representando celula viva e morta, respectivamente.
 * Input: 
 *      n    -> dimensão do tabuleiro;
 *      grid -> tabuleiro
 * 
 * Output:
 *      vazio
 */
void printGrid(int n, int **grid)
{
    int i, j;
    for(i = 0; i < n; i++)
    {
        for(j = 0; j < n; j++)
        {
            printf("%d ", grid[i][j]);
        }
        printf("\n");
    }
}

/** Funcao que verifica quantos vizinhos de uma celula estao vivos.
 *  Input:
 *       grid -> tabuleiro;
 *       i    -> linha da celula;
 *       j    -> coluna da celula;
 *       n    -> dimensão do tabuleiro
 * 
 * Output:
 *       cont -> quantidade de vizinhos vivos
 */
int getNeighbors(int **grid, int i, int j, int n)
{
    //contador que guardará a quantidade de células vizinhas
    int cont = 0;

    //variáveis de apoio, caso a célula esteja na borda do tabuleiro
    int x1, y1, x2, y2;

//verificações das extremidades do tabuleiro
    //1. se a célula está na borda direita do tabuleiro
    //2. se não 1, verifica se está na borda esquerda
    //3. posição da coluna ok
    if(j == (n-1))
    {
        x1 = j - 1;
        x2 = 0;
    }
    else if(j == 0)
    {
        x1 = n - 1;
        x2 = j + 1;
    }
    else
    {
        x1 = j - 1;
        x2 = j + 1;
    }

    //4. se a célula está na borda superior
    //5. se não 4, verifica se está na borda inferior
    //6. posição da linha ok
    if(i == 0)
    {
        y1 = n - 1;
        y2 = i + 1;
    }
    else if(i == (n-1))
    {
        y1 = i - 1;
        y2 = 0;
    }
    else
    {
        y1 = i - 1;
        y2 = i + 1;
    }
// fim da verificação dos vizinhos da célula

//conta quantos vizinhos estão vivos
    if(grid[y1][x1] == 1) cont++; // superior esquerdo
    if(grid[y1][x2] == 1) cont++; // superior direito
    if(grid[y1][j]  == 1) cont++; // superior central
    if(grid[y2][x1] == 1) cont++; // inferior esquerdo
    if(grid[y2][x2] == 1) cont++; // inferior direito
    if(grid[y2][j]  == 1) cont++; // inferior central
    if(grid[i][x1]  == 1) cont++; // central esquerdo
    if(grid[i][x2]  == 1) cont++; // central direito

// retorna a quantidade de vizinhos vivos
    return cont;
}

int main(int argc, char *argv[])
{

    int n;
    int i, x, y, g, j; 
    int **grid;
    int **newGrid;
    int generations;
    int neighbors;
    double t1, t2;
    int alive = 0;
    int threads;
    int print_grid;
    int aux = 0;
    int id;
    int grid_serial[n*n];

    n = 2048;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &id);
    printf("ID: %d\n", id);
    //Alocação dinâmica para as matrizes dos tabuleiros
    grid = (int**) malloc(sizeof(int*) * n);
    newGrid = (int**) malloc(sizeof(int*) * n);

    for(i = 0; i < n; i++)
    {
        grid[i] = (int*) malloc(sizeof(int) * n);
        newGrid[i] = (int*) malloc(sizeof(int) * n);
    }

    //GLIDER
    int lin = 1, col = 1;
    grid[lin][col+1] = 1;
    grid[lin+1][col+2] = 1;
    grid[lin+2][col] = 1;
    grid[lin+2][col+1] = 1;
    grid[lin+2][col+2] = 1;
    
    //R-pentomino
    lin =10; col = 30;
    grid[lin][col+1] = 1;
    grid[lin][col+2] = 1;
    grid[lin+1][col] = 1;
    grid[lin+1][col+1] = 1;
    grid[lin+2][col+1] = 1;

    alive = 10;
    
    generations = 100;
    print_grid = 0;
//inicia o jogo, realiza X iteracoes
    double start;
    double end;
    int inicio, fim, block_size;
    MPI_Comm_size(MPI_COMM_WORLD, &threads);
    block_size = (n * n) / threads;

    int pos_inicio[threads];
    int pos_fim[threads];
    int displaicimentos[4];
    int counts[4] = {2048,2048,2048,2048};
    
    inicio = id * block_size;
    pos_inicio[id] = inicio;
    fim = inicio + block_size;
    pos_fim[id] = fim;
    //printf("Linha: %d\nColuna: %d\nId: %d\n\n------\n", pos_inicio[i]/n, fim%n, i);
    

    int last_line;
    for(g = 0; g < generations; g++)
    {
        //percorre cada posicao do tabuleiro verificando os vizinhos vivos
        //grid[i][j]
        aux = 0;
        if(id == 0)
        {
            aux = alive;
            MPI_Bcast(&g, 1, MPI_INT, 0, MPI_COMM_WORLD);
            MPI_Bcast(&alive, 1, MPI_INT, 0, MPI_COMM_WORLD);
        }
        //printf("Id: %d\nLinha: %d\nColuna: %d\n\n------\n",id,pos_inicio[id]/n,pos_inicio[id]%n);
        for(i = pos_inicio[id]; i < pos_fim[id]; i++)
        {
            y = (i / n);
            x = (i % n);
            //obtem os vizinhos vivos da celula grid[i][j]

            neighbors = getNeighbors(grid, y, x, n);
            //1. se a celula ja esta viva
            //2. se nao 1, celula esta morta
            if(grid[y][x] == 1)
            {
                //1. tem menos que dois vizinhos = MORRE
                //2. tem mais que 3 vizinhos = MORRE
                //3. 2 ou 3 vizinhos = VIVE
                if(neighbors < 2)
                {
                    
                    aux = aux - 1;
                    
                    newGrid[y][x] = 0;
                }
                else if(neighbors > 3)
                {
                    
                    aux = aux - 1;
                    
                    newGrid[y][x] = 0;
                }
                else
                {
                    
                    newGrid[y][x] = 1;
                }
            }
            else
            {
                //1. se tem 3 vizinhos = REVIVE
                if(neighbors == 3)
                {
                    //printf("ok");
                    aux = aux + 1;
                    
                    newGrid[y][x] = 1;
                }
            }
        }  
        // printf("ID: %d\n AUX: %d\n----\n", id, aux);
        // fim da geração, nova geração se torna a atual para próxima iteração
        //A - 0 -> 511
        // i = 0 -> i = 2048 * 511
        //B - 512 -> 1023
        //C - 1024 -> 1535
        //D - 1536 -> 2047
        for(i = pos_inicio[id]; i < pos_fim[id]; i = i + n)
        {
            
            y = (i / n);
            displaicimentos[id] = y;
            MPI_Allgatherv(newGrid[y], n, MPI_INT, grid, counts, displaicimentos, MPI_INT, MPI_COMM_WORLD);
            
            if(id == 0){
                //printf("\n\n");
                //printGrid(50, newGrid);
                //printf("-> %d %d\n", newGrid[2][1], y);
            }
            //x = (i % n);
            //MPI_Allgather(&newGrid[y][x], 1, MPI_INT, &grid[y][x], 1, MPI_INT, MPI_COMM_WORLD);   
            if(y == 2)
            {
                //printf("-> %d\n", newGrid[2][1]);
                //printf("TESTE\n");
                for(j = 0; j < 50; j++)
                {
                   // printf("%d ", newGrid[y][j]);
                }
                //printf("\n-----\n\n");
            }
            // **newgrid
            //MPI_Allgather(newGrid[y], n, MPI_INT, grid[y], n, MPI_INT, MPI_COMM_WORLD);
            //newGrid[0] -> grid[0]
            //newGrid[1] -> grid[1]
            //newGrid[2] -> grid[2]
        }
        //printf("Thread: %d\nGeneration: %d\n Alive: %d\n\n -------\n", id, g, alive);
        // geração atual atualizada, exibe quantas celulas vivas
        MPI_Reduce(&aux, &alive, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
        //MPI_Barrier(MPI_COMM_WORLD);
        if(id == 0)
        {
            printf("\n---------GEN %d : %d celulas vivas---------\n", g+1, alive);
            //printGrid(50, grid);
        }
    }
    MPI_Finalize();
    //printf("Tempo de execucao: %f, %f, %f", (end - start)+(t2 - t1), (end - start), (t2 - t1));
    return 0;
}